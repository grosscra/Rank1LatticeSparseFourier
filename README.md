# Sparse Fourier Transform Techniques for Rank-1 Lattices

This repository hosts code used in the numerics section of the paper **Sparse Fourier Transforms on Rank 1 Lattices for the Rapid and Low-Memory Approximation of Functions of Many Variables**. In particular, we include implementations of Algorithm 1 and Algorithm 2.

## Instructions

1.	Clone the repository *including its submodules*:

    ```
	git clone --recurse-submodules https://gitlab.com/grosscra/Rank1LatticeSparseFourier.git
	```

2. (Optional) Clone grosscra/SFTDiscretizerMATLAB> into the top level of the repository 
3. Run any script in the `figure` directory to produce the data used in that figure.

## Notes

The code corresponding to Algorithm 1 and Algorithm 2 can be found in in `algorithms/phase_enc.m` and `algorithms/two_dim.m`. The implemenation for Algorithm 2 is parallelized using MATLAB's parfor.
Documentation is provided and (after being added to the path) can be accessed via `help phase_enc` and `help two_dim` respectively. 

Note that to use either of these implementations, at least `SublinearSparseFourierMATLAB` must be on the path.
To use the discrete algorithm provided by `SFTDiscretizerMATLAB`, the directories `SFTDiscretizerMATLAB/DSFTs`and `SFTDiscretizerMATLAB/Filters` must also be on the path.

See `tests/test_phase_hc.m` and `tests/test_2d_hc.m` for minimal working examples with trigonometric polynomials.

## Contributions 

The majority of the testing code and initial algorithm implementations are by [Toni Volkmer](https://www-user.tu-chemnitz.de/~tovo/index.php.en). The updated implementations making use of grosscra/SublinearSparseFourierMATLAB> and grosscra/SFTDiscretizerMATLAB> as well as the associated testing setup was written by Craig Gross.
