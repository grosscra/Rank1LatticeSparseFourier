clear;
addpath('../algorithms');
addpath('../tests');
addpath('../tests/helper_functions');
addpath('../SublinearSparseFourierMATLAB');
addpath('../SFTDiscretizerMATLAB');
warning('off', 'SFTPlan:outsideBand');
warning('off', 'SFTPlan:tooFewCoeffs');

for algo_c={'phase_sftrand'}; 
	filename=sprintf('test1_bspline10_%s_2s_ps0_rs005.mat', algo_c{1});
	test1_2s_bspline10_hc_multipletests(...
		33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
		[100,200,500,1000,2000,4000], algo_c{1}, 100, filename, 'randomScale', 0.05, 'primeShift', 0 );
end
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_b10;
fprintf('\n');

for algo_c={'phase_sftrand'}; 
	filename=sprintf('test1_bspline10_%s_2s_ps0_rs01.mat', algo_c{1});
	test1_2s_bspline10_hc_multipletests(...
		33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
		[100,200,500,1000,2000,4000], algo_c{1}, 100, filename, 'randomScale', 0.1, 'primeShift', 0 );
end
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_b10;
fprintf('\n');

for algo_c={'phase_sftrand'}; 
	for snr=[10,20,30];
		filename=sprintf('test1_bspline10_%s_2s_ps0_rs01_snr%d.mat', algo_c{1}, snr);
		test1_2s_bspline10_hc_multipletests(...
			33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
			[100,200,500,1000,2000,4000], algo_c{1}, 100, filename, 'randomScale', 0.1, 'primeShift', 0, 'snr', snr );
		clearvars -except filename snr algo_c;
		load(filename);
		fprintf('=== %s ===\n', filename);
		print_b10;
		fprintf('\n');
	end
end
