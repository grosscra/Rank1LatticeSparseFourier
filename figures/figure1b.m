clear;
addpath('../algorithms');
addpath('../tests');
addpath('../tests/helper_functions');
addpath('../SublinearSparseFourierMATLAB');
addpath('../SFTDiscretizerMATLAB');
warning('off', 'SFTPlan:outsideBand');
warning('off', 'SFTPlan:tooFewCoeffs');


% Phase encoding

% d = 10
Ns=[16,16,16,16,16,16,16,16,16,15];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 11
Ns=[repmat(13,1,4),repmat(12,1,7)];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 12
Ns=repmat(10,1,12);
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 13
Ns=[10,repmat(9,1,8),7,7,7,7];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 14
Ns=[repmat(8,1,3),repmat(7,1,11)];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 15
Ns=[repmat(7,1,5),repmat(6,1,10)];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 16
Ns=[repmat(6,1,13),repmat(5,1,1),4,4];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 17
Ns=[repmat(6,1,4),repmat(5,1,11),4,4];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 18
Ns=[repmat(5,1,12),repmat(4,1,6)];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 19
Ns=[repmat(5,1,6),repmat(4,1,13)];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 20
Ns=[5,repmat(4,1,18),3];
d=length(Ns);
filename=sprintf('test3cube_phase_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, 'phase_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');


% 2d DFT

% d = 10
Ns=[16,16,16,16,16,16,16,16,16,15];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 11
Ns=[repmat(13,1,4),repmat(12,1,7)];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 12
Ns=repmat(10,1,12);
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 13
Ns=[10,repmat(9,1,8),7,7,7,7];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 14
Ns=[repmat(8,1,3),repmat(7,1,11)];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 15
Ns=[repmat(7,1,5),repmat(6,1,10)];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 16
Ns=[repmat(6,1,13),repmat(5,1,1),4,4];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 17
Ns=[repmat(6,1,4),repmat(5,1,11),4,4];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 18
Ns=[repmat(5,1,12),repmat(4,1,6)];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 19
Ns=[repmat(5,1,6),repmat(4,1,13)];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

% d = 20
Ns=[5,repmat(4,1,18),3];
d=length(Ns);
filename=sprintf('test3cube_2dim_sftrand_2s_ps0_rs03_Nalgo16_1e12_d%d.mat',d);
test3cube_multipletests(...
	d, 16, Ns, 100, '2dim_sftrand', 100, filename,...
	'randomScale', 0.3, 'primeShift', 0, 'bandwidth', prod(Ns), 's_scale', 2 );
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');
