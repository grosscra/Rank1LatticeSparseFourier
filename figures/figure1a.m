clear;
addpath('../algorithms');
addpath('../tests');
addpath('../tests/helper_functions');
addpath('../SublinearSparseFourierMATLAB');
addpath('../SFTDiscretizerMATLAB');
warning('off', 'SFTPlan:outsideBand');
warning('off', 'SFTPlan:tooFewCoeffs');

for algo_c={'phase_sftrand'}; 
	filename=sprintf('test1b_%s_2s_ps0_rs03.mat', algo_c{1});
	test1_multipletests(...
		10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
		[10,20,50,100,200,500,1000], algo_c{1}, 100, filename, 'randomScale', 0.3, 'primeShift', 0, 's_scale', 2 );
end
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

for algo_c={'phase_sftrand'};
	filename=sprintf('test1b_%s_2s_bwl1_ps0_rs03.mat', algo_c{1});
	test1_multipletests(...
		10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
		[10,20,50,100,200,500,1000], algo_c{1}, 100, filename, 'randomScale', 0.3, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 's_scale', 2 ); 
end
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

for algo_c={'phase_sftrand'};
	filename=sprintf('test1b_%s_2s_bwl1_ps0_rs05.mat', algo_c{1});
	test1_multipletests(...
		10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
		[10,20,50,100,200,500,1000], algo_c{1}, 100, filename, 'randomScale', 0.5, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 's_scale', 2 );
end
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

for algo_c={'2dim_sftrand'};
	filename=sprintf('test1b_%s_2s_bwl1_ps0_rs03.mat', algo_c{1}); 
	test1_multipletests(...
		10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
		[10,20,50,100], algo_c{1}, 100, filename, 'randomScale', 0.3, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 's_scale', 2 ); end
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');

for algo_c={'2dim_sftrand'};
	filename=sprintf('test1b_%s_2s_bwl1_ps0_rs05.mat', algo_c{1});
	test1_multipletests(...
		10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
		[10,20,50,100], algo_c{1}, 100, filename, 'randomScale', 0.5, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 's_scale', 2 );
end
clearvars -except filename;
load(filename);
fprintf('=== %s ===\n', filename);
print_1;
fprintf('\n');
