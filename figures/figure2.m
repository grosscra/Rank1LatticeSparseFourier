clear;
addpath('../algorithms');
addpath('../tests');
addpath('../tests/helper_functions');
addpath('../SublinearSparseFourierMATLAB');
addpath('../SFTDiscretizerMATLAB');
warning('off', 'SFTPlan:outsideBand');
warning('off', 'SFTPlan:tooFewCoeffs');

for algo_c={'phase_sftrand'};
	for snr=[0,5,10,15,20,25,30];
		filename=sprintf('test1b_%s_2s_bwl1_ps0_s100_rs03_snr%d.mat', algo_c{1}, snr);
		test1_multipletests(...
			10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
			[100], algo_c{1}, 100, filename, 'randomScale', 0.3, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 'snr', snr, 's_scale', 2 );
		clearvars -except filename snr algo_c;
		load(filename);
		fprintf('=== %s ===\n', filename);
		print_1;
		fprintf('\n');
	end;
end

for algo_c={'phase_sftrand'};
	for snr=[0,5,10,15,20,25,30];
		filename=sprintf('test1b_%s_2s_bwl1_ps0_s100_rs05_snr%d.mat', algo_c{1}, snr);
		test1_multipletests(...
			10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
			[100], algo_c{1}, 100, filename, 'randomScale', 0.5, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 'snr', snr, 's_scale', 2 );
		clearvars -except filename snr algo_c;
		load(filename);
		fprintf('=== %s ===\n', filename);
		print_1;
		fprintf('\n');
	end;
end

for algo_c={'2dim_sftrand'};
	for snr=[0,5,10,15,20,25,30];
		filename=sprintf('test1b_%s_2s_bwl1_ps0_s100_rs03_snr%d.mat', algo_c{1}, snr);
		test1_multipletests(...
			10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
			[100], algo_c{1}, 100, filename, 'randomScale', 0.3, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 'snr', snr, 's_scale', 2 );
		clearvars -except filename snr algo_c;
		load(filename);
		fprintf('=== %s ===\n', filename);
		print_1;
		fprintf('\n');
	end;
end

for algo_c={'2dim_sftrand'};
	for snr=[0,5,10,15,20,25,30];
		filename=sprintf('test1b_%s_2s_bwl1_ps0_s100_rs05_snr%d.mat', algo_c{1}, snr);
		test1_multipletests(...
			10, 16, 33, [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128], 2040484044, ...
			[100], algo_c{1}, 100, filename, 'randomScale', 0.5, 'primeShift', 0, 'bandwidth', 2*320144128*(16+9)+1, 'snr', snr, 's_scale', 2 );
		clearvars -except filename snr algo_c;
		load(filename);
		fprintf('=== %s ===\n', filename);
		print_1;
		fprintf('\n');
	end;
end
