fprintf('randomScale');
for ii=1:size(results,1)
  fprintf(' & s=%d', sparsity_s_array(ii));
end
fprintf('\n');

fprintf('%g, samples ratio:', randomScale);
for ii=1:size(results,1)
  if length([results(ii,:).num_samples]) ~= size(results,2); continue; end
  samples_ratio = sum([results(ii,:).num_samples]./[results(ii,:).M]) / size(results,2);
  fprintf(' & %.3f', samples_ratio);
end
fprintf(' \\\\\n');

fprintf('%g, samples:', randomScale);
for ii=1:size(results,1)
  if length([results(ii,:).num_samples]) ~= size(results,2); continue; end
  samples_avg = sum([results(ii,:).num_samples]) / size(results,2);
  fprintf(' & %.3f', samples_avg);
end
fprintf(' \\\\\n');

for ii=1:size(results,1)
  if length([results(ii,:).num_samples]) ~= size(results,2); continue; end
  samples_avg = sum([results(ii,:).num_samples]) / size(results,2);
  fprintf(' (%d,%.3f)', sparsity_s_array(ii), samples_avg);
end
fprintf(' %%%g, avg samples \n', randomScale);

fprintf('%g, success rate:', randomScale);
for ii=1:size(results,1)
  if length([results(ii,:).num_samples]) ~= size(results,2); continue; end
  success_rate = sum([results(ii,:).num_intersect]==sparsity_s_array(ii)) / size(results,2);
  fprintf(' & %.2f', success_rate);
end
fprintf(' \\\\\n');

for ii=1:size(results,1)
  if length([results(ii,:).num_samples]) ~= size(results,2); continue; end
  success_rate = sum([results(ii,:).num_intersect]==sparsity_s_array(ii)) / size(results,2);
  fprintf(' (%d,%.2f)', sparsity_s_array(ii), success_rate);
end
fprintf(' %%%g, success rate \n', randomScale);

fprintf('%g, avg.\\ freq.\\ detected:', randomScale);
for ii=1:size(results,1)
  if length([results(ii,:).num_samples]) ~= size(results,2); continue; end
  avg_detected = sum([results(ii,:).num_intersect]) / size(results,2);
  fprintf(' & %.2f', avg_detected);
end
fprintf(' \\\\\n');

for ii=1:size(results,1)
  if length([results(ii,:).num_samples]) ~= size(results,2); continue; end
  err_l2_rel = sum([results(ii,:).err_l2_rel]) / size(results,2);
  fprintf(' (%d,%.3e)', sparsity_s_array(ii), err_l2_rel);
end
fprintf(' %%%g, err_l2_rel \n', randomScale);
