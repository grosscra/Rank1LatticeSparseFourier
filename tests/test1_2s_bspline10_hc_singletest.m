function [ num_freq_out, error2, error_trunc2, error_alias2, num_samples, runtimes ] ...
  = test1_2s_bspline10_hc_singletest( N_algo, z, M, algorithm_name, num_freq, ...
       primeList, C, alpha, beta, primeShift, sigma, randomScale, bandwidth, snr)

%% ===Run phase encoding===
% Arguments below: z, M, N_algo, s are self-explanatory
%	'deterministic': Use a deterministic algorithm
%	'true': Use discrete algorithm (DMSFTPlan)
%	'true': Use mexExecute in any SFTPlan
%	fc: True coefficients for fast sampling
%	freq: True frequencies for fast sampling
%	Remainder are options to be passed to the DMSFTPlan.
%
%	Hint: 'help phase_enc' for doc/syntax

switch algorithm_name
  case {'phase_sftdet','phase_dmsftdet','2dmin_sftdet','2dim_dmsftdet'}
    executionMode = 'deterministic';
  case {'phase_sftrand','phase_dmsftrand','2dim_sftrand','2dim_dmsftrand'}
    executionMode = 'random';
  otherwise
    error('Unsupported algorithm');
end

switch algorithm_name
  case {'phase_sftdet','phase_sftrand','2dim_sftdet','2dim_sftrand'}
    discrete = false;
  case {'phase_dmsftdet','phase_dmsftrand','2dim_dmsftdet','2dim_dmsftrand'}
    discrete = true;
  otherwise
    error('Unsupported algorithm');
end

switch algorithm_name
  case {'phase_sftdet','phase_sftrand','phase_dmsftdet','phase_dmsftrand'}
    [freq_detected, fhat_tilde, num_samples, runtimes] = phase_enc_fct(z, M, N_algo, num_freq, executionMode, discrete, false, @bspline_test_10d,...
	    'primeList', primeList, 'C', C, 'alpha', alpha, 'beta', beta, 'primeShift', primeShift, 'sigma', sigma, 'randomScale', randomScale, 'bandwidth', bandwidth, 'snr', snr);
%   case {'2dim_sftdet','2dim_sftrand','2dim_dmsftdet','2dim_dmsftrand'}
%     [freq_detected, fc_detected, num_samples, runtimes] = two_dim(z, M, N_algo, num_freq, executionMode, discrete, false, fc, freq,...
%       'primeList', primeList, 'C', 0.05, 'alpha', 1, 'beta', 1, 'primeShift', primeShift, 'sigma', sigma, 'randomScale', randomScale);
  otherwise
    error('Unsupported algorithm');
end

if size(freq_detected,1) > num_freq
  [~,iisort] = sort(abs(fhat_tilde), 'descend');
  fhat_tilde = fhat_tilde(iisort(1:num_freq));
  freq_detected = freq_detected(iisort(1:num_freq),:);
end

num_freq_out = size(freq_detected,1);

fhat = bspline_test_10d_fouriercoeff(freq_detected);
norm_fct_square = 3.86052137015856367;
error_trunc2 = sqrt(norm_fct_square - sum(abs(fhat).^2)) / sqrt(norm_fct_square);
error_alias2 = norm(fhat-fhat_tilde) / sqrt(norm_fct_square);
error2 = sqrt(norm_fct_square - sum(abs(fhat).^2) + norm(fhat-fhat_tilde)^2) / sqrt(norm_fct_square);

