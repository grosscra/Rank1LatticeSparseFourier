function [ results ] = test1_2s_bspline10_hc_multipletests( N_algo, z, M, num_freq_array, algorithm_name, nruns, filename, varargin )

% addpath('helper_functions');

%% ===Algorithm===
if ~exist('primeList') || length(primeList) < 10000
	primeList = primes(104740); % First 10,000 primes
end
% Arguments below: z, M, N_algo, s are self-explanatory
%	'deterministic': Use a deterministic algorithm
%	'true': Use discrete algorithm (DMSFTPlan)
%	'true': Use mexExecute in any SFTPlan
%	fc: True coefficients for fast sampling
%	freq: True frequencies for fast sampling
%	Remainder are options to be passed to the DMSFTPlan.

[C, alpha, beta, primeShift, sigma, randomScale, bandwidth, snr] = ...
				variableInputHandler(varargin, 'C', 'alpha', 'beta', 'primeShift', 'sigma', 'randomScale', 'bandwidth', 'snr');

if isempty(C); C = 1; end
if isempty(alpha); alpha = 1; end
if isempty(beta); beta = 1; end
if isempty(primeShift); primeShift = 50; end
if isempty(sigma); sigma = 2/3; end
if isempty(randomScale); randomScale = 1; end
if isempty(snr); snr = inf; end

results = repmat(struct(), length(num_freq_array), nruns);

for ii_s = 1:length(num_freq_array)
  rng('default');
  num_freq = num_freq_array(ii_s);
  for ir = 1:nruns
	  disp(snr);
    [ num_freq_out, error2, error2_trunc, error2_alias, num_samples, runtimes ] ...
      = test1_2s_bspline10_hc_singletest( N_algo, z, M, algorithm_name, num_freq, ...
          primeList, C, alpha, beta, primeShift, sigma, randomScale, bandwidth, snr);
    
    % fprintf('N_algo = %d, s_in = %d, s_out = %d, ir = %d, M = %d, samples = %d | %.1f%%, ', ...
      %N_algo, num_freq, num_freq_out, ir, M, num_samples, num_samples/M*100);

    % fprintf('rel_err_2 = %.3e (trunc = %.1e, alias = %.1e)\n', error2, error2_trunc, error2_alias);

      results(ii_s,ir).num_freq = num_freq;
      results(ii_s,ir).num_freq_out = num_freq_out;
      results(ii_s,ir).err_l2_rel = error2;
      results(ii_s,ir).err_l2_rel_trunc = error2_trunc;
      results(ii_s,ir).err_l2_rel_alias = error2_alias;
      results(ii_s,ir).z = z;
      results(ii_s,ir).M = M;
      results(ii_s,ir).num_samples = num_samples;
      results(ii_s,ir).runtimes = runtimes; % all, sampling
      save(filename, 'N_algo', 'algorithm_name', 'nruns', 'num_freq_array', 'results', 'C', 'alpha', 'beta', 'primeShift', 'sigma', 'randomScale', 'bandwidth', 'snr');
  end
end

end

function [ varargout ] = variableInputHandler(inputs, varargin)
%VARIABLEINPUTHANDLER- Extracts Name, Value inputs from a varargin cell array
%In addition to the inputs cell array of Name, Value pairs, accepts strings 
%corresponding to names to extract from the cell array.
%
% Syntax:  [ varargout ] = variableInputHandler(inputs, ...)
%     with optional trailing string arguments.
%
% Inputs:
%    inputs: A one-dimensional cell array containing strings/character vectors 
%		representing names followed by the value of that property.
%    varargin: Optional strings representing the names of properties to be 
%		extracted from inputs.
%
% Outputs:
%    varargout: For each name given in varargin, will output the 
%		corresponding value as given in inputs. If the name does not exist in 
%		inputs, an empty array will be returned in that position. Checks should 
%		be made for empty arrays in order to assign default values if necessary.

% Author: Craig Gross
% Mar 2020

if nargout ~= length(varargin)
	error('Number of outputs must match number of names input.');
end

names = cell(1, length(inputs) / 2);
for i = 1:2:length(inputs)
	if isstring(inputs{i})
		names{(i + 1) / 2} = char(inputs{i});
	elseif ischar(inputs{i})
		names{(i + 1) / 2} = inputs{i};
	else
		error('Name of input should be character array or string.');
	end
end

for i = 1:length(varargin)
	namesIndex = find(contains(names, varargin{i}));
	if isempty(namesIndex)
		varargout{i} = []; 
	else
		varargout{i} = inputs{2 * namesIndex};
	end
end

end
