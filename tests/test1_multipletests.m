function [ results ] = test1_multipletests( d, N_lattice_param, N_algo, z, M, sparsity_s_array, algorithm_name, nruns, filename, varargin )

% addpath('helper_functions');

freq_all = coord_symhc2(N_lattice_param, d);

%% ===Algorithm===
if ~exist('primeList') || length(primeList) < 10000
	primeList = primes(104740); % First 10,000 primes
end
% Arguments below: z, M, N_algo, s are self-explanatory
%	'deterministic': Use a deterministic algorithm
%	'true': Use discrete algorithm (DMSFTPlan)
%	'true': Use mexExecute in any SFTPlan
%	fc: True coefficients for fast sampling
%	freq: True frequencies for fast sampling
%	Remainder are options to be passed to the DMSFTPlan.

[C, alpha, beta, primeShift, sigma, randomScale, bandwidth, snr, s_scale] = ...
				variableInputHandler(varargin, 'C', 'alpha', 'beta', 'primeShift', 'sigma', 'randomScale', 'bandwidth', 'snr', 's_scale');

if isempty(C); C = 1; end
if isempty(alpha); alpha = 1; end
if isempty(beta); beta = 1; end
if isempty(primeShift); primeShift = 50; end
if isempty(sigma); sigma = 2/3; end
if isempty(randomScale); randomScale = 1; end
if isempty(s_scale); s_scale = 1; end

results = repmat(struct(), length(sparsity_s_array), nruns);

for ii_s = 1:length(sparsity_s_array)
  rng('default');
  sparsity_s = sparsity_s_array(ii_s);
  for ir = 1:nruns
    num_freq = sparsity_s;
    freq = freq_all(randperm(size(freq_all,1),sparsity_s), :);
    [ ~, num_freq_out, num_intersect, max_err_abs, err_l2_rel, num_samples, runtimes ] ...
      = test1_singletest( N_algo, freq_all, z, M, algorithm_name, freq, s_scale*sparsity_s, ...
        primeList, C, alpha, beta, primeShift, sigma, randomScale, bandwidth, snr);
    % fprintf('N_algo = %d, s = %d, s_algo = %d, ir = %d, M = %d, samples = %d | %.1f%%, ', ...
    %   N_algo, sparsity_s, s_scale*sparsity_s, ir, M, num_samples, num_samples/M*100);

    % if num_intersect == num_freq
    %   fprintf('all %d detected, rel_err_2 = %.3e\n', num_freq, err_l2_rel);
    % else
    %   fprintf('only %d out of %d detected\n', num_intersect, num_freq);
    % end

      results(ii_s,ir).num_freq = num_freq;
      results(ii_s,ir).num_freq_out = num_freq_out;
      results(ii_s,ir).num_intersect = num_intersect;
      results(ii_s,ir).max_err_abs = max_err_abs;
      results(ii_s,ir).err_l2_rel = err_l2_rel;
      results(ii_s,ir).z = z;
      results(ii_s,ir).M = M;
      results(ii_s,ir).num_samples = num_samples;
      results(ii_s,ir).runtimes = runtimes; % all, sampling
      save(filename, 'd', 'N_lattice_param', 'N_algo', 'algorithm_name', 'nruns', 'sparsity_s_array', 's_scale', 'results', 'C', 'alpha', 'beta', 'primeShift', 'sigma', 'randomScale', 'bandwidth', 'snr');
  end
end

end

function [ varargout ] = variableInputHandler(inputs, varargin)
%VARIABLEINPUTHANDLER- Extracts Name, Value inputs from a varargin cell array
%In addition to the inputs cell array of Name, Value pairs, accepts strings 
%corresponding to names to extract from the cell array.
%
% Syntax:  [ varargout ] = variableInputHandler(inputs, ...)
%     with optional trailing string arguments.
%
% Inputs:
%    inputs: A one-dimensional cell array containing strings/character vectors 
%		representing names followed by the value of that property.
%    varargin: Optional strings representing the names of properties to be 
%		extracted from inputs.
%
% Outputs:
%    varargout: For each name given in varargin, will output the 
%		corresponding value as given in inputs. If the name does not exist in 
%		inputs, an empty array will be returned in that position. Checks should 
%		be made for empty arrays in order to assign default values if necessary.

% Author: Craig Gross
% Mar 2020

if nargout ~= length(varargin)
	error('Number of outputs must match number of names input.');
end

names = cell(1, length(inputs) / 2);
for i = 1:2:length(inputs)
	if isstring(inputs{i})
		names{(i + 1) / 2} = char(inputs{i});
	elseif ischar(inputs{i})
		names{(i + 1) / 2} = inputs{i};
	else
		error('Name of input should be character array or string.');
	end
end

for i = 1:length(varargin)
	namesIndex = find(contains(names, varargin{i}));
	if isempty(namesIndex)
		varargout{i} = []; 
	else
		varargout{i} = inputs{2 * namesIndex};
	end
end

end
