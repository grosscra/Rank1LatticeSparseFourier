function [ num_freq, num_freq_out, num_intersect, max_err_abs, err_l2_rel, num_samples, runtimes ] ...
  = test3cube_singletest( N_algo, z, M, algorithm_name, freq, sparsity_s, ...
       primeList, C, alpha, beta, primeShift, sigma, randomScale, bandwidth, snr)

%% ===Get random s-sparse trig polynomial===
fc = [];
while length(fc) < size(freq,1)
  fc = [fc; 2*rand(size(freq,1),1) - 1 + 1i * (2*rand(size(freq,1),1) - 1)];
  fc(abs(fc) < 1e-3) = [];
end
fc = fc(1:size(freq,1));
% fc = 2*rand(size(freq,1),1) - 1 + 1i * (2*rand(size(freq,1),1) - 1);
% ii = find(abs(fc) < 1e-3);
% fc(ii) = 1e-3 * exp(2*pi*1i*rand(length(ii),1));
% [~, ind] = sort(abs(fc), 'descend');
% fc = fc(ind);

old_rng = rng;
%% ===Run phase encoding===
% Arguments below: z, M, N_algo, s are self-explanatory
%	'deterministic': Use a deterministic algorithm
%	'true': Use discrete algorithm (DMSFTPlan)
%	'true': Use mexExecute in any SFTPlan
%	fc: True coefficients for fast sampling
%	freq: True frequencies for fast sampling
%	Remainder are options to be passed to the DMSFTPlan.
%
%	Hint: 'help phase_enc' for doc/syntax

switch algorithm_name
  case {'phase_sftdet','phase_dmsftdet','2dmin_sftdet','2dim_dmsftdet'}
    executionMode = 'deterministic';
  case {'phase_sftrand','phase_dmsftrand','2dim_sftrand','2dim_dmsftrand'}
    executionMode = 'random';
  otherwise
    error('Unsupported algorithm');
end

switch algorithm_name
  case {'phase_sftdet','phase_sftrand','2dim_sftdet','2dim_sftrand'}
    discrete = false;
  case {'phase_dmsftdet','phase_dmsftrand','2dim_dmsftdet','2dim_dmsftrand'}
    discrete = true;
  otherwise
    error('Unsupported algorithm');
end

switch algorithm_name
  case {'phase_sftdet','phase_sftrand','phase_dmsftdet','phase_dmsftrand'}
    [freq_detected, fc_detected, num_samples, runtimes] = phase_enc(z, M, N_algo, sparsity_s, executionMode, discrete, false, fc, freq,...
	    'primeList', primeList, 'C', C, 'alpha', alpha, 'beta', beta, 'primeShift', primeShift, 'sigma', sigma, 'randomScale', randomScale, 'bandwidth', bandwidth, 'snr', snr);
  case {'2dim_sftdet','2dim_sftrand','2dim_dmsftdet','2dim_dmsftrand'}
    [freq_detected, fc_detected, num_samples, runtimes] = two_dim(z, M, N_algo, sparsity_s, executionMode, discrete, false, fc, freq,...
      'primeList', primeList, 'C', C, 'alpha', alpha, 'beta', beta, 'primeShift', primeShift, 'sigma', sigma, 'randomScale', randomScale, 'bandwidth', bandwidth, 'snr', snr);
  otherwise
    error('Unsupported algorithm');
end

num_freq = sparsity_s;
num_freq_out = size(freq_detected,1);

% ===Report results===
[~, ia, ib] = intersect(freq, freq_detected, 'rows');

num_intersect = length(ia);

% if length(ia) == size(freq,1)
%   fprintf('all %d freq detected, fc rel_error_2 = %.3e\n', size(freq,1), norm(fc(ia)-fc_detected(ib))/norm(fc));
% else
%   fprintf('only %d out of %d freq detected\n', length(ia), size(freq,1));
% end

% fprintf('number of samples = %d, %.1f%% of full R1LFFT\n', num_samples, num_samples/M*100);

long_fhat = fc(:);
long_fhat_out = zeros(size(long_fhat));
long_fhat_out(ia) = fc_detected(ib);

freq_neg = setdiff(freq_detected,freq,'rows');

if ~isempty(freq_neg)
  [~, ~, ib_ianeg] = intersect(freq_neg,freq_detected,'rows');
  long_fhat = [long_fhat; zeros(length(ib_ianeg),1)];
  long_fhat_out = [long_fhat_out; fc_detected(ib_ianeg)];
end

max_err_abs = max(abs(long_fhat-long_fhat_out));
err_l2_rel = norm(long_fhat-long_fhat_out,2) / norm(long_fhat,2);

rng(old_rng);
