function [ val ] = bspline_o4( x )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

% normalization: none => f0=1/27, norm_l2=11/4860
%                27   => f0=1,    norm_l2=33/20=1.65

x = x - floor(x);

val = ones(size(x,1),1);

for t=1:size(x,2)
  ind = find(x(:,t)<0);
  if ~isempty(ind)
    val(ind) = 0;
  end
  
  ind = find((0<=x(:,t)).*(x(:,t)<1/4));
  if ~isempty(ind)
    val(ind) = val(ind) .* 128/3.*x(ind,t).^3;
  end

  ind = find((1/4<=x(:,t)).*(x(:,t)<2/4));
  if ~isempty(ind)
    val(ind) = val(ind) .* (8/3-32*x(ind,t)+128*x(ind,t).^2-128*x(ind,t).^3);
  end

  ind = find((2/4<=x(:,t)).*(x(:,t)<3/4));
  if ~isempty(ind)
    val(ind) = val(ind) .* (-88/3-256*x(ind,t).^2+160*x(ind,t)+128*x(ind,t).^3);
  end

  ind = find((3/4<=x(:,t)).*(x(:,t)<1));
  if ~isempty(ind)
    val(ind) = val(ind) .* (128/3-128*x(ind,t)+128*x(ind,t).^2-(128/3)*x(ind,t).^3);
  end
  
  ind = find(x(:,t)>=1);
  if ~isempty(ind)
    val(ind) = 0;
  end
  
%  val = 27 * val;
  val = sqrt(315/604) * val;
end

end

