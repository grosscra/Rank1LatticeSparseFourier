function [ val ] = bspline_o2( x )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

x = x - floor(x);

val = ones(size(x,1),1);

for t=1:size(x,2)
  ind = find(x(:,t)<0);
  if ~isempty(ind)
    val(ind) = 0;
  end
  
  ind = find((0<=x(:,t)).*(x(:,t)<1/2));
  if ~isempty(ind)
    val(ind) = val(ind) .* 4.* x(ind,t);
  end

  ind = find((1/2<=x(:,t)).*(x(:,t)<1));
  if ~isempty(ind)
    val(ind) = val(ind) .* 4 .* (1-x(ind,t));
  end

  ind = find(x(:,t)>=1);
  if ~isempty(ind)
    val(ind) = 0;
  end
  
  val = sqrt(3/4) * val;
end

end

