function [ val ] = bspline_sinc( x )
%BSPLINE_SINC Summary of this function goes here
%   Detailed explanation goes here

if size(x,2) ~= 1
  error('only column vectors are supported');
end

val = ones(size(x));
ind = find(x~=0);
val(ind) = sin(x(ind))./x(ind);

end

