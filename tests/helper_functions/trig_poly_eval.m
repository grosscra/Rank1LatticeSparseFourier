function [ out ] = trig_poly_eval( freq, val, x )
%TRIG_POLY_EVAL Summary of this function goes here
%   Detailed explanation goes here

out = zeros(size(x,1),1);

for ifreq = 1:size(freq,1)
  out = out + val(ifreq) * exp(2i*pi*x*freq(ifreq,:)');
end

end

