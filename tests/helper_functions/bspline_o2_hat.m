function [ val ] = bspline_o2_hat( k )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

val = ones(size(k,1),1);

for t=1:size(k,2)
  ind = find(k(:,t)~=0);
  if ~isempty(ind)
    val(ind) = val(ind) .* bspline_sinc(pi/2*k(ind,t)).^2 .* cos(pi*k(ind,t));
  end

  val = sqrt(3/4) * val;
end

end

