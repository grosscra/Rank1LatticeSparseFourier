function [ fhat, norm_fct_square ] = bspline_test_10d_fouriercoeff( freq_out )
%BSPLINE_TEST_10D_FOURIERCOEFF Fourier coefficients of bspline_test_10d.
%
% Copyright (c) 2015 Toni Volkmer

fhat = zeros(size(freq_out,1),1);

ind=find(sum(abs(freq_out(:,[2 4 5 6 7 9 10])),2)==0);
fhat(ind) = fhat(ind) + bspline_o2_hat(freq_out(ind,[1 3 8]));

ind=find(sum(abs(freq_out(:,[1 3 4 7 8 9])),2)==0);
fhat(ind) = fhat(ind) + bspline_o4_hat(freq_out(ind,[2 5 6 10]));

ind=find(sum(abs(freq_out(:,[1 2 3 5 6 8 10])),2)==0);
fhat(ind) = fhat(ind) + bspline_o6_hat(freq_out(ind,[4 7 9]));

norm_fct_square = 3 + 2*bspline_o2_hat(zeros(1,3))*bspline_o4_hat(zeros(1,4)) + 2*bspline_o2_hat(zeros(1,3))*bspline_o6_hat(zeros(1,3)) + 2*bspline_o4_hat(zeros(1,4))*bspline_o6_hat(zeros(1,3));
end


function [ val ] = bspline_o2_hat( k )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

val = ones(size(k,1),1);

for t=1:size(k,2)
  ind = find(k(:,t)~=0);
  if ~isempty(ind)
    val(ind) = val(ind) .* bspline_sinc(pi/2*k(ind,t)).^2 .* cos(pi*k(ind,t));
  end

%   ind = find(k(:,t)==0);
%   val(ind) = val(ind) .* 1;
  val = sqrt(3/4) * val;
end

end


function [ val ] = bspline_o4_hat( k )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

val = ones(size(k,1),1);

for t=1:size(k,2)
  ind = find(k(:,t)~=0);
  if ~isempty(ind)
    val(ind) = val(ind) .* bspline_sinc(pi/4*k(ind,t)).^4 .* cos(pi*k(ind,t));
  end

%   ind = find(k(:,t)==0);
%   val(ind) = val(ind) .* 1;
  val = sqrt(315/604) * val;
end

end


function [ val ] = bspline_o6_hat( k )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

val = ones(size(k,1),1);

for t=1:size(k,2)
  ind = find(k(:,t)~=0);
  if ~isempty(ind)
    val(ind) = val(ind) .* bspline_sinc(pi/6*k(ind,t)).^6 .* cos(pi*k(ind,t));
  end

%   ind = find(k(:,t)==0);
%   val(ind) = val(ind) .* 1;
  val = sqrt(277200/655177) * val;
end

end

function [ val ] = bspline_sinc( x )
%BSPLINE_SINC Summary of this function goes here
%   Detailed explanation goes here

if size(x,2) ~= 1
  error('only column vectors are supported');
end

val = ones(size(x));
ind = find(x~=0);
val(ind) = sin(x(ind))./x(ind);

end
