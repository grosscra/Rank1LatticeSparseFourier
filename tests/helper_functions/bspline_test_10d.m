function [ valout ] = bspline_test_10d( x )
%BSPLINE_TEST_10D 10-dimensional test function consisting of tensor-products of B-splines.
%
% Copyright (c) 2015 Toni Volkmer

if size(x,2) ~= 10
  error('input must be M x 10 matrix');
end

valout = bspline_o2(x(:,[1 3 8])) + bspline_o4(x(:,[2 5 6 10])) + bspline_o6(x(:,[4 7 9]));
end


function [ val ] = bspline_o2( x )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

x = x - floor(x);

val = ones(size(x,1),1);

for t=1:size(x,2)
  ind = find((0<=x(:,t)).*(x(:,t)<1/2));
  if ~isempty(ind)
    val(ind) = val(ind) .* 4.* x(ind,t);
  end

  ind = find((1/2<=x(:,t)).*(x(:,t)<1));
  if ~isempty(ind)
    val(ind) = val(ind) .* 4 .* (1-x(ind,t));
  end
  
  val = sqrt(3/4) * val;
end

end


function [ val ] = bspline_o4( x )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

% normalization: none => f0=1/27, norm_l2=11/4860
%                27   => f0=1,    norm_l2=33/20=1.65

x = x - floor(x);

val = ones(size(x,1),1);

for t=1:size(x,2)
  ind = find((0<=x(:,t)).*(x(:,t)<1/4));
  if ~isempty(ind)
    val(ind) = val(ind) .* 128/3.*x(ind,t).^3;
  end

  ind = find((1/4<=x(:,t)).*(x(:,t)<2/4));
  if ~isempty(ind)
    val(ind) = val(ind) .* (8/3-32*x(ind,t)+128*x(ind,t).^2-128*x(ind,t).^3);
  end

  ind = find((2/4<=x(:,t)).*(x(:,t)<3/4));
  if ~isempty(ind)
    val(ind) = val(ind) .* (-88/3-256*x(ind,t).^2+160*x(ind,t)+128*x(ind,t).^3);
  end

  ind = find((3/4<=x(:,t)).*(x(:,t)<1));
  if ~isempty(ind)
    val(ind) = val(ind) .* (128/3-128*x(ind,t)+128*x(ind,t).^2-(128/3)*x(ind,t).^3);
  end
  
%  val = 27 * val;
  val = sqrt(315/604) * val;
end

end


function [ val ] = bspline_o6( x )
%bspline_o2 quadratic bspline
%   Detailed explanation goes here

% normalization: none => f0=1/27, norm_l2=11/4860
%                27   => f0=1,    norm_l2=33/20=1.65

x = x - floor(x);

val = ones(size(x,1),1);

for t=1:size(x,2)
  ind = find((0<=x(:,t)).*(x(:,t)<1/6));
  if ~isempty(ind)
    val(ind) = val(ind) .* ((1944/5)*x(ind,t).^5);
  end

  ind = find((1/6<=x(:,t)).*(x(:,t)<2/6));
  if ~isempty(ind)
    val(ind) = val(ind) .* (3/10-9*x(ind,t)+108*x(ind,t).^2-648*x(ind,t).^3+1944*x(ind,t).^4-1944*x(ind,t).^5);
  end

  ind = find((2/6<=x(:,t)).*(x(:,t)<3/6));
  if ~isempty(ind)
    val(ind) = val(ind) .* (-237/10+351*x(ind,t)-2052*x(ind,t).^2+5832*x(ind,t).^3-7776*x(ind,t).^4+3888*x(ind,t).^5);
  end

  ind = find((3/6<=x(:,t)).*(x(:,t)<4/6));
  if ~isempty(ind)
    val(ind) = val(ind) .* (2193/10+7668*x(ind,t).^2-2079*x(ind,t)+11664*x(ind,t).^4-13608*x(ind,t).^3-3888*x(ind,t).^5);
  end

  ind = find((4/6<=x(:,t)).*(x(:,t)<5/6));
  if ~isempty(ind)
    val(ind) = val(ind) .* (-5487/10+3681*x(ind,t)-9612*x(ind,t).^2+12312*x(ind,t).^3-7776*x(ind,t).^4+1944*x(ind,t).^5);
  end

  ind = find((5/6<=x(:,t)).*(x(:,t)<1));
  if ~isempty(ind)
    val(ind) = val(ind) .* (1944/5-1944*x(ind,t)+3888*x(ind,t).^2-3888*x(ind,t).^3+1944*x(ind,t).^4-(1944/5)*x(ind,t).^5);
  end

%  val = 27 * val;
  val = sqrt(277200/655177) * val;
end

end
