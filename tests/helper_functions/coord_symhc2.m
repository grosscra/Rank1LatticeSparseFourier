function [ freq ] = coord_symhc2( N, d )
%COORD_SYMHC2 - Indices of d-dimensional hyperbolic cross with expansion N
%A frequency k is contained in the constructed symmetric hyperbolic cross 
% if prod(max(1, abs(k))) <= N.
%
% Syntax: [ freq ] = coord_symhc2( N, d )
%
% Inputs:
%   N - Expansion of hyperbolic cross
%   d - Dimension of hyperbolic cross
%
% Outputs:
%   freq - |HC| * d array where each row is a frequency vector contained in
%       specified symmetric hyperbolic cross

if ceil(N) ~= N
  error('N must be integer');
end

Nmax_array = [0 0 4096 4096 512 256 128 128 32 32];

freq = [];

if ((d <= size(Nmax_array,2)) && (N <= Nmax_array(d)))
  filename = sprintf('coord_symhc_N%d_d%d.mat',N,d);
  try
    load(filename,'freq');
    fprintf('Loading from %s\n',filename);
  catch err
  end
  if ((size(freq,1) == 0) || (size(freq,2) ~= d))
    filename = sprintf('coord_symhc_N%d_d%d.mat',Nmax_array(d),d);
    try
      load(filename,'freq');
      fprintf('Loading from %s\n',filename);
    catch err
    end
    if ((size(freq,1) > 0) && (size(freq,2) == d))
      freq = freq(prod(max(1,abs(freq)),2) <= N,:);
    end
  end
  freq = double(freq);
end

if ((size(freq,1) == 0) || (size(freq,2) ~= d))

freq = (-N:N)';

for t=2:d
  tmpweight = prod(max(1,abs(freq)),2);
  newfreq = [freq, zeros(size(freq,1),1)];
  for newcomp = 1:N
    ind = find(tmpweight*max(1,abs(newcomp)) <= N);
    newfreq = [newfreq; freq(ind,:), repmat(-newcomp, length(ind), 1); freq(ind,:), repmat(newcomp, length(ind), 1)];
  end
  freq = newfreq;
end

end

end

