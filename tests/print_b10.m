fprintf('randomScale');
for ii=1:size(results,1)
  fprintf(' & 2s=%d', num_freq_array(ii));
end
fprintf('\n');

fprintf('%g, samples ratio:', randomScale);
for ii=1:size(results,1)
  samples_ratio = sum([results(ii,:).num_samples]./[results(ii,:).M]) / size(results,2);
  fprintf(' & %.3f', samples_ratio);
end
fprintf(' \\\\\n');

fprintf('%g, samples:', randomScale);
for ii=1:size(results,1)
  samples_ratio = sum([results(ii,:).num_samples]) / size(results,2);
  fprintf(' & %.3f', samples_ratio);
end
fprintf(' \\\\\n');

fprintf('%g, rel.\\ $\\ell_2$ error:', randomScale);
for ii=1:size(results,1)
  avg_error2 = sum([results(ii,:).err_l2_rel]) / size(results,2);
  fprintf(' & %.1e', avg_error2);
end
fprintf(' \\\\\n');
