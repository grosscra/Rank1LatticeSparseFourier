% ===Imports===
addpath('../algorithms');
addpath('../tests');
addpath('../tests/helper_functions');
addpath('../SublinearSparseFourierMATLAB');
addpath('../SFTDiscretizerMATLAB/DSFTs');
addpath('../SFTDiscretizerMATLAB/Filters');
warning('off', 'SFTPlan:outsideBand');
warning('off', 'SFTPlan:tooFewCoeffs');

%===Setup frequency set and rank-1 lattice===

% N = 16; freq_all = coord_symhc2_parfor(N,10);
% z = [1 33 579 3628 21944 169230 1105193 7798320 49768670 320144128];
% M = 2040484044;
%
%d = 3;
%N_algo = 8;
%N_coord_symhc = floor(N_algo/2);
%freq_all = coord_symhc2(N_coord_symhc,3);
%z = [1 17 163];
%M = 1035;

d = 4;
N_algo = 128;
N_coord_symhc = floor(N_algo/2);
freq_all = coord_symhc2(N_coord_symhc,d);
z = [1 129 8451 47463]; M = 475829; % recoR1L for N<=129 and d=4
if mod(N_algo,2) == 0
  freq_all(max(freq_all,[],2) >= N_algo/2,:) = [];
end

if length(unique(mod(freq_all*z',M))) ~= size(freq_all,1)
  error('rank-1 lattice is not a reconstructing one');
end

% === Get random s-sparse trig polynomial===
s = 10;
freq = freq_all(randperm(size(freq_all,1),s), :);
fc = 2*rand(s,1) - 1 + 1i * (2*rand(s,1) - 1);
fc(abs(fc) < 1e-1) = 1e-1 * exp(2*pi*1i*rand);
[~, ind] = sort(abs(fc), 'descend');
fc = fc(ind);

% ===Run two dimensional SFT/FFT recovery===
if ~exist('primeList') || length(primeList) < 10000
	primeList = primes(104740); % First 10,000 primes
end
% Arguments below: z, M, N_algo, s are self-explanatory
%	'deterministic': Use a deterministic algorithm
%	'true': Use discrete algorithm (DMSFTPlan)
%	'true': Use mexExecute in any SFTPlan
%	fc: True coefficients for fast sampling
%	freq: True frequencies for fast sampling
%	Remainder are options to be passed to the DMSFTPlan.
%
%	Hint: 'help phase_enc' for doc/syntax

tic
[freq_detected, fc_detected, num_samples] = two_dim(z, M, N_algo, s, 'random', false, false, fc, freq,...
	'primeList', primeList, 'C', 0.05, 'alpha', 1, 'beta', 1, 'primeShift', 20);
toc

% ===Report results===
[~, ia, ib] = intersect(freq, freq_detected, 'rows');

if length(ia) == size(freq,1)
  fprintf('all %d freq detected, fc rel_error_2 = %.3e\n', size(freq,1), norm(fc(ia)-fc_detected(ib))/norm(fc));
else
  fprintf('only %d out of %d freq detected\n', length(ia), size(freq,1));
end

fprintf('number of samples = %d, %.1f%% of full R1LFFT\n', num_samples, num_samples/M*100);
