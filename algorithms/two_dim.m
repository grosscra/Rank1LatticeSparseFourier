function [freq_detected, fc, num_samples, runtimes] = two_dim(z, M, N, s, executionMode, discrete, parallelSFT, true_coefs, true_freq, varargin)
%two_dim- Find frequencies with 2D SFT/FFT pairs.
%Only works with a generated trigonometric polynomial (for sampling purposes).
%
% Syntax: [freq_detected, fc] = two_dim(z, M, ...
%                                N, s, true_coefs, true_freq, samples_cell)
%
% Inputs:
%   z - Rank-1 lattice generating vector of dimension d
%   M - Size of rank-1 lattice
%   N - Bandwidth: Fourier support is contained in 
%       (-ceil(N / 2), floor(N / 2)]^d
%   s - size of Fourier support
%   executionMode - whether to use a deterministic or random execution.
%		Should be string either 'deterministic' or 'random'.
%   discrete - boolean, to use a discrete (true) or nonequispaced plan (false).
%   parallelSFT - boolean, whether or not to use a parallel SFT execution.
%   true_coefs - the true Fourier coefficients of the test trig polynomial.
%   true_freq - the corresponding frequencies of the test trig polynomial.
%   Optional arguments - Options to pass to the plan in Name, Value pairs, e.g.
%			'C', C
%			'primeList', primeList
%			'sigma', sigma
%		For DMSFT only:
%			'alpha', alpha
%			'beta', beta
%		See SFTPlan.SFTPlan and DMSFTPlan.DMSFTPlan for details.
%
% Outputs:
%   freq_detected - s * d array of frequencies of function recovered.
%   fc - Associated Fourier coefficients of detected frequencies.
%   num_samples - The total number of samples used.
%

runtimes = zeros(2,1);

timerTotal = tic;

d = length(z);
if discrete
	bandwidth = M;
else
	% TODO: Add way to use variable bandwidth parameter in this function
	bandwidth = d * N * M;
end

snr = inf;

for i=1:length(varargin)
  if strcmpi(varargin{i}, 'bandwidth')
    if ~isempty(varargin{i+1})
      % fprintf('overriding bandwidth from %g to %g\n', bandwidth, varargin{i+1});
      bandwidth = varargin{i+1};
    end
%     varargin(i:i+1) = [];
%     i = i-2;
  elseif strcmpi(varargin{i}, 'snr')
    if ~isempty(varargin{i+1})
      % fprintf('setting snr to %g\n', varargin{i+1});
      snr = varargin{i+1};
    end
%     varargin(i:i+1) = [];
%     i = i-2;
  end
end

% Setup SFTPlan
% addpath('../SublinearSparseFourierMATLAB/');
% addpath('../SFTDiscretizerMATLAB/Filters');
% addpath('../SFTDiscretizerMATLAB/DSFTs');
% warning('off', 'SFTPlan:outsideBand');
% warning('off', 'SFTPlan:tooFewCoeffs');


% Lattice SFT
one_d_freq = z * true_freq.';

if discrete
	plan = DMSFTPlan(bandwidth, s, executionMode, 'parallelSFT', parallelSFT, varargin{:}); 
else
	plan = SFTPlan(bandwidth, s, executionMode, varargin{:});
end


% If not using a trig polynomial, get the samples and sample the polynomial, e.g.,
%
% if discrete
% 	nodes = (0:(N - 1))' ./ N * reshape(z, 1, []);
% else
% 	nodes = reshape(plan.nodes, [], 1) * reshape(z, 1, []);
% end
% samples = f(nodes); % (if f is vectorized for row vector input, and columns of points)
timerSamples = tic;
if discrete
	nodes = plan.necessaryNodes;
	samples = trig_poly_eval(one_d_freq(:), true_coefs(:), nodes(:));
else
  nodes = []; %plan.nodes;
	samples = plan.sampleTrigPolynomial(true_coefs, one_d_freq);
end
if ~isinf(snr)
  noise_values = randn(length(samples),1) + 1i*randn(length(samples),1);
  res_const=norm(samples)^2/norm(noise_values)^2*10^(-snr/10);
  samples = samples + noise_values * sqrt(res_const);
end
num_samples = length(samples);
runtimes(2) = toc(timerSamples);

if ~discrete && parallelSFT
	% TODO: Add a parallelSFT parameter to SFTPlan to determine execution method.
	[fc, frequencies] = plan.mexExecute(samples);
else
	[fc, frequencies] = plan.execute(samples);
end

fc = reshape(fc, [], 1);

freq_detected = nan(s,d);

for l = 1:d
	lattice_SFT_freqs = cell(N, 1);
	lattice_SFT_coefs = cell(N ,1);

	z_prime = z; z_prime(l) = 0;
	e_l = zeros(1,d); e_l(l) = 1;
	one_d_freq = z_prime * true_freq.';

	timerSamples= cell(N, 1);
	runtime_arr = zeros(N, 1);
	% This loop 'parfor's nicely, but I don't have any clue about the thread 
	% safety of using the same plan for each worker.
	parfor h=0:N-1
		warning('off', 'SFTPlan:outsideBand');
		warning('off', 'SFTPlan:tooFewCoeffs');
		% This is cheating here:
		% Rather than fixing one input, we exploit trig polynomials and apply
		% evaluation on that input to the coefficients. By changing one_d_freq
		% above for each l, we ensure that we're not evaluating along the lattice
		% in this chosen dimension.
		one_d_coefs = true_coefs .* exp((2i * pi * h / N) .* true_freq(:, l));
		
		% If not using a trig polynomial, get the samples and sample the function 
		% at the dimension-l frozen nodes, e.g.,
		%
		% shift_nodes = nodes;
		% shift_nodes(:, l) = h / N;
		% samples = f(shift_nodes); % (if f is vectorized for length d-row vectors)
		% timerSamples = tic;
		timerSamples{h + 1} = tic;
		if discrete
			samples = trig_poly_eval(one_d_freq(:), one_d_coefs(:), nodes(:));
		else
			samples = plan.sampleTrigPolynomial(one_d_coefs, one_d_freq);
		end
		if ~isinf(snr)
		  noise_values = randn(length(samples),1) + 1i*randn(length(samples),1);
		  res_const=norm(samples)^2/norm(noise_values)^2*10^(-snr/10);
		  samples = samples + noise_values * sqrt(res_const);
		end
        num_samples = num_samples + length(samples);
		% runtimes(2) = runtimes(2) + toc(timerSamples);
		runtime_arr(h + 1) = toc(timerSamples{h + 1});

		if ~discrete & parallelSFT
			% TODO: Add a parallelSFT parameter to SFTPlan to determine execution method.
			[new_coefs, new_freqs] = plan.mexExecute(samples);
		else
			[new_coefs, new_freqs] = plan.execute(samples);
		end
		lattice_SFT_freqs{h + 1} = new_freqs;
		lattice_SFT_coefs{h + 1} = new_coefs;
	end
	runtimes(2) = sum(runtime_arr);

	% Collect all recovered frequencies from the above SFTs.
	% This can easily be done in the previous loop, but separating out allows for
	% the possibility of a parfor above.
	total_lattice_SFT_freqs = [];
	for h = 1:N
		total_lattice_SFT_freqs = union(total_lattice_SFT_freqs, lattice_SFT_freqs{h});
	end

	% Align all previously recovered coefficients in a matrix to FFT down the columns.
	new_FFT_vals = zeros(N, length(total_lattice_SFT_freqs));
	for h = 1:N
		[total_idx, local_freqs] = ismember(total_lattice_SFT_freqs, lattice_SFT_freqs{h});
		new_FFT_vals(h, total_idx) = lattice_SFT_coefs{h}(local_freqs(local_freqs ~= 0));
	end
	
	% Perform FFT down columns
	fc_2d_l = fft(new_FFT_vals, N, 1) / N;
	fc_2d_l = fftshift(fc_2d_l, 1);

	Ns_array = (-ceil((N-1)/2):floor((N-1)/2))';

	% Take 2s largest values in fc_2d_l
	[~, fc_2d_l_idx] = maxk(reshape(fc_2d_l, [], 1), 2 * s);
	[row, col] = ind2sub(size(fc_2d_l), fc_2d_l_idx); 

	% Link the original coefficients to their calculated counterparts in fc_2d_l 
	% and use the rows (propery fftshifted with Ns_array) as the lth dimension 
	% of the recovered frequencies.
	[~, ia, ib] = intersect(mod(frequencies, bandwidth), mod(Ns_array(row) * z(l) + total_lattice_SFT_freqs(col), bandwidth), 'stable');
	freq_detected(ia, l) = Ns_array(row(ib));

	% TODO: Could be rewritten so that the synchronization based coefficients 
	% comparisison as in line (XV) is done, but shouldn't be much different.

end

runtimes(1) = toc(timerTotal);

end


