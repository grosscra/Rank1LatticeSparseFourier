function [freq_detected, fc, num_samples, runtimes] = phase_enc_fct(z, M, N, s, executionMode, discrete, parallelSFT, fct_handle, varargin)
%phase_enc_fct- Phase encoding to find frequencies
%Uses (DM)SFTPlan to find the frequencies of a trigonometric polynomial sampled
%on a rank-1 lattice via phase encoding.
%
%
% Syntax: [freq_detected, fc] = phase_enc(z, M, N, s, executionMode, ...
%									discrete, parallelSFT, true_coefs, ...
%									true_freq, ...)
%
% Inputs:
%   z - Rank-1 lattice generating vector of dimension d
%   M - Size of rank-1 lattice
%   N - Bandwidth: Fourier support is contained in 
%       (-ceil(N / 2), floor(N / 2)]^d
%   s - size of Fourier support
%   executionMode - whether to use a deterministic or random execution.
%		Should be string either 'deterministic' or 'random'.
%   discrete - boolean, to use a discrete (true) or nonequispaced plan (false).
%   parallelSFT - boolean, whether or not to use a parallel SFT execution.
%   true_coefs - the true Fourier coefficients of the test trig polynomial.
%   true_freq - the corresponding frequencies of the test trig polynomial.
%   Optional arguments - Options to pass to the plan in Name, Value pairs, e.g.
%			'C', C
%			'primeList', primeList
%			'sigma', sigma
%		For DMSFT only:
%			'alpha', alpha
%			'beta', beta
%		See SFTPlan.SFTPlan and DMSFTPlan.DMSFTPlan for details.
%
% Outputs:
%   freq_detected - s * d array of frequencies of function recovered
%   fc - Associated Fourier coefficients of detected frequencies
%   num_samples - The total number of function samples used.
%   runtimes - vector of runtimes, entry 1: total, 2: sampling

runtimes = zeros(2,1);
timerTotal = tic;

d = length(z);

if discrete
	bandwidth = M;
else
	% TODO: Add way to use variable bandwidth parameter in this function
	bandwidth = d * N * M;
%     bandwidth = max(z) * N;
end

snr = inf;

for i=1:length(varargin)
  if strcmpi(varargin{i}, 'bandwidth')
    if ~isempty(varargin{i+1})
      % fprintf('overriding bandwidth from %g to %g\n', bandwidth, varargin{i+1});
      bandwidth = varargin{i+1};
    end
%     varargin(i:i+1) = [];
%     i = i-2;
  elseif strcmpi(varargin{i}, 'snr')
    if ~isempty(varargin{i+1})
      % fprintf('setting snr to %g\n', varargin{i+1});
      snr = varargin{i+1};
    end
%     varargin(i:i+1) = [];
%     i = i-2;
  end
%   i = i+2;
end

ind_l_s = cell(d,1);
fc_l_s = cell(d,1);

%Setup SFTPlan
% addpath('../SublinearSparseFourierMATLAB');
% addpath('../SFTDiscretizerMATLAB/Filters');
% addpath('../SFTDiscretizerMATLAB/DSFTs');
% warning('off', 'SFTPlan:outsideBand');
% warning('off', 'SFTPlan:tooFewCoeffs');


% Perform SFT on function restricted to rank-1 lattice

if discrete
	plan = DMSFTPlan(bandwidth, s, executionMode, 'parallelSFT', parallelSFT, varargin{:});
else
	plan = SFTPlan(bandwidth, s, executionMode, varargin{:});
end

num_samples = 0;

timerSampling = tic;
if discrete
  nodes = plan.necessaryNodes;
else
	nodes = reshape(plan.nodes, [], 1) * reshape(z, 1, []);
end
samples = fct_handle(nodes);
if ~isinf(snr)
  noise_values = randn(length(samples),1) + 1i*randn(length(samples),1);
  res_const=norm(samples)^2/norm(noise_values)^2*10^(-snr/10);
  samples = samples + noise_values * sqrt(res_const);
end
num_samples = num_samples + length(samples);
runtimes(2) = toc(timerSampling);

if ~discrete & parallelSFT
	% TODO: Add a parallelSFT parameter to SFTPlan to determine execution method.
	[fc, ind_1d] = plan.mexExecute(samples);
else
	[fc, ind_1d] = plan.execute(samples);
end

fc = reshape(fc, [], 1);

freq_detected = nan(length(fc),d);

for l = 1:d
	% Perform SFT on restriction of shifted function
	
	% If not using a trig polynomial, get the samples and sample the function 
	% at the shifted nodes, e.g.,
	%
timerSampling = tic;
	shift_nodes = nodes;
	shift_nodes(:, l) = mod(shift_nodes(:, l) + 1 / N, 1);
	samples = fct_handle(shift_nodes); % (if f is vectorized for length d-row vectors)
if ~isinf(snr)
  noise_values = randn(length(samples),1) + 1i*randn(length(samples),1);
  res_const=norm(samples)^2/norm(noise_values)^2*10^(-snr/10);
  samples = samples + noise_values * sqrt(res_const);
end
num_samples = num_samples + length(samples);
runtimes(2) = runtimes(2) + toc(timerSampling);

	if ~discrete & parallelSFT
		% TODO: Add a parallelSFT parameter to SFTPlan to determine execution method.
		[fc_l, ind_l] = plan.mexExecute(samples);
	else
		[fc_l, ind_l] = plan.execute(samples);
	end
	ind_l_s{l} = ind_l; % Store frequencies
	fc_l_s{l} = reshape(fc_l, [], 1); % Store coefficients

	% Find common frequencies of unshifted and shifted 1d function
	[ind, ia, ib] = intersect(ind_1d, ind_l_s{l}, 'stable');
	ind_1d = ind;
	fc = fc(ia);
	fc_l_s{l} = fc_l_s{l}(ib);

	freq_detected = freq_detected(ia, :);

	% Use the phase shift to recover the lth component of the d-dim freq
	freq_detected(:,l) = round(real(log(fc_l_s{l} ./ fc) / (2*pi*1i / N)));
end

% TODO: Could rewrite so that the check in Alg 1, line (X) is performed rather
% than discarding mismatched frequencies. But shouldn't be much different as is.

if mod(N, 2) == 0
  freq_detected(freq_detected==N/2) = -N/2;
end

[freq_detected,ia] = unique(freq_detected,'rows');
fc = fc(ia);

runtimes(1) = toc(timerTotal);


end

